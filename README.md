## Avris Stringer -- Micrus bridge ##

Dokumentacja Stringera dostępna jest pod adresem **[docs.avris.it/stringer](http://docs.avris.it/stringer)**.

Aby zainstalować bridge dla frameworka [Micrus](http://micrus.avris.it), w pliku `app/Config/modules.yml` dodaj linijkę:

     - Avris\Stringer\Micrus\StringerModule

a następnie wykonaj polecenie w konsoli polecenie:

	composer require avris/stringer-micrus

Wszystkie funkcje biblioteki są dostępne zarówno w widoku jako filtry Twiga,
jak i w innych miejscach kodu -- jako serwis.

    $vocative = $this->getService('stringer')->vocative('Michał');
