<?php
namespace Avris\Stringer\Micrus;

use Avris\Micrus\Module;
use Avris\Stringer\Stringer;
use Avris\Stringer\Twig\StringerTwigExtension;

class StringerModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'stringer' => [
                    'class' => Stringer::class,
                ],
                'stringerTwig' => [
                    'class' => StringerTwigExtension::class,
                    'parameters' => ['@stringer'],
                    'tags' => ['twigExtension'],
                ],
            ],
        ];
    }
}